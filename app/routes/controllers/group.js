const router = require("express").Router();

const Group = require("../../db/model/Group");
const Message = require("../../db/model/Group");

/**
 * @swagger
 * /group/list:
 *   post:
 *     summary: Get Group list.
 *     responses:
 *       200:
 *         description: Get List
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: group
 *                   properties:
 *                    id:
 *                      type: Number
 *                      required: true
 *                    name:
 *                      type: String
 *                      required: true
 *                    schedule:
 *                      type: String
 *                      required: true
 *                    type:
 *                      type: String
 *                      required: true
 *                    startDate:
 *                      type: Date
 *                      required: true
 *                    endDate:
 *                      type: Date
 *                      required: true
 */
router.get("/list", function (req, res, next) {
  Group.find(function (error, groups) {
    if (error) {
      return next(new Error(error));
    }

    res.json(groups); // return all groups
  });
});

router.get("/list", function (req, res, next) {
  const session = req.body.session;
  Group.find({ session: session }, function (error, groups) {
    if (error) {
      return next(new Error(error));
    }

    res.json(groups); // return all groups with a specific session
  });
});

// get all group items in the db with specific schedule
router.get("/list", function (req, res, next) {
  const schedule = req.body.schedule;
  Group.find({ schedule: schedule }, function (error, groups) {
    if (error) {
      return next(new Error(error));
    }

    res.json(groups); // return all groups with a specific schedule
  });
});

module.exports = router;
