const router = require("express").Router();

const Organisation = require("../../db/model/Organisation");

// get all organisation items in the db
router.get("/list", function (req, res, next) {
  Organisation.find(function (error, organisations) {
    if (error) {
      return next(new Error(error));
    }

    res.json(organisations); // return all organisations
  });
});

// add a organisation item
router.post("/add", function (req, res) {
  Organisation.create(
    {
      name: req.body.name,
    },
    function (error, organisation) {
      if (error) {
        //TODO add multilanguage mangement for message
        console.error(error);
        res.status(400).send("Unable to create an organisation");
      }
      res.status(200).json(organisation);
    }
  );
});

// get an organisation item
router.get("/get/:id", function (req, res, next) {
  var id = req.params.id;
  Organisation.findById(id, function (error, organisation) {
    if (error) {
      //TODO add multilanguage management for error message
      console.error(error);
      return next(new Error("Organisation was not found"));
    }
    //TODO add multilanguage management for message
    res.json(organisation);
  });
});

// delete an organisation item
router.get("/delete/:id", function (req, res, next) {
  var id = req.params.id;
  Organisation.findByIdAndRemove(id, function (error, organisation) {
    if (error) {
      //TODO add multilanguage management for error message
      console.error(error);
      return next(new Error("Organisation was not found"));
    }
    //TODO add multilanguage management for message
    res.json("Successfully removed");
  });
});

// update an organisation item
router.post("/update/:id", function (req, res, next) {
  var id = req.params.id;
  Organisation.findById(id, function (error, organisation) {
    if (error) {
      //TODO add multilanguage management for error message
      console.error(error);
      return next(new Error("The Organisation was not found"));
    } else {
      organisation.name = req.body.name;

      organisation
        .save()
        .then((organisation) => res.json(organisation))
        .catch((err) => res.status(400).json(err));
    }
  });
});

module.exports = router;
