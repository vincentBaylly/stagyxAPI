const router = require("express").Router();

const Task = require("../../db/model/Task");

// get all task items in the db
router.get("/list", function (req, res, next) {
  Task.find(function (error, tasks) {
    if (error) {
      return next(new Error(error));
    }

    res.json(tasks); // return all tasks
  });
});

// get all task items in the db with specific user
router.get("/list", function (req, res, next) {
  const authorId = req.body.author;
  Task.find({ authorId: authorId }, function (error, tasks) {
    if (error) {
      return next(new Error(error));
    }

    res.json(tasks); // return all tasks
  });
});

// add a task item
router.post("/add", function (req, res) {
  Task.create(
    {
      authorId: req.body.authorId,
      description: req.body.description,
      autoEval: req.body.autoEval,
      tutorEval: req.body.tutorEval,
      managerEval: req.body.managerEval,
      level: req.body.level,
      startDate: req.body.startDate,
      endDate: req.body.endDate,
      status: req.body.status,
    },
    function (error, task) {
      if (error) {
        //TODO add multilanguage mangement for message
        console.error(error);
        res.status(400).send("Unable to create Task");
      }
      res.status(200).json(task);
    }
  );
});

// get an task item
router.get("/get/:id", function (req, res, next) {
  var id = req.params.id;
  Task.findById(id, function (error, task) {
    if (error) {
      //TODO add multilanguage management for error message
      console.error(error);
      return next(new Error("Task was not found"));
    }
    //TODO add multilanguage management for message
    res.json(task);
  });
});

// delete an task item
router.get("/delete/:id", function (req, res, next) {
  var id = req.params.id;
  Task.findByIdAndRemove(id, function (error, task) {
    if (error) {
      //TODO add multilanguage management for error message
      console.error(error);
      return next(new Error("Task was not found"));
    }
    //TODO add multilanguage management for message
    res.json("Successfully removed");
  });
});

// update an task item
router.post("/update/:id", function (req, res, next) {
  var id = req.params.id;
  Task.findById(id, function (error, task) {
    if (error) {
      //TODO add multilanguage management for error message
      console.error(error);
      return next(new Error("The Task was not found"));
    } else {
      task.authorId = req.body.authorId;
      task.description = req.body.description;
      task.autoEval = req.body.autoEval;
      task.tutorEval = req.body.tutorEval;
      task.managerEval = req.body.managerEval;
      task.level = req.body.level;
      task.startDate = req.body.startDate;
      task.endDate = req.body.endDate;
      task.status = req.body.status;

      task
        .save()
        .then((task) => res.json(task))
        .catch((err) => res.status(400).json(err));
    }
  });
});

module.exports = router;
