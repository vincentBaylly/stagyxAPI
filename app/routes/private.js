const routes = require("express")();

//registers our authentication routes with Express.
routes.use("/organisation", require("./controllers/organisation"));
routes.use("/group", require("./controllers/group"));
routes.use("/users", require("./controllers/users"));
routes.use("/task", require("./controllers/task"));

module.exports = routes;
