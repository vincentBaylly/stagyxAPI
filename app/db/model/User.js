const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const UserSchema = new Schema({
  name: String,
  emailAddress: {
    type: String,
    required: true,
    unique: true,
  },
  userName: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  //001-coordinator
  //002-tutor
  //003-manager
  //004-intership
  role: {
    type: String,
    required: true,
  },
  organisationId: Number,
  lastConnection: {
    type: Date,
    required: true,
  },
});
module.exports = User = mongoose.model("User", UserSchema);
