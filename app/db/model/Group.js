const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const GroupSchema = new Schema({
  id: {
    type: Number,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  schedule: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
});
module.exports = Group = mongoose.model("Group", GroupSchema);
