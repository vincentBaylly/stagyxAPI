const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const PartOfSchema = new Schema({
  program_id: {
    type: Number,
    required: true,
  },
  session_id: {
    type: Number,
    required: true,
  },
  group_id: {
    type: Number,
    required: true,
  },
});
module.exports = PartOf = mongoose.model("PartOf", PartOfSchema);
