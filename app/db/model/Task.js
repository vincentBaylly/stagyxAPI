const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const TaskSchema = new Schema({
  authorId: {
    type: String,
    required: true,
  },
  description: String,
  autoEval: Number,
  tutorEval: Number,
  managerEval: Number,
  level: Number,
  startDate: Date,
  endDate: Date,
  status: String,
});
module.exports = Task = mongoose.model("Task", TaskSchema);
