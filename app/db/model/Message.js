const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const MesageSchema = new Schema({
  content: {
    type: String,
    required: true,
  },
  groupId: {
    type: Number,
    required: true,
  },
});
module.exports = Message = mongoose.model("Message", MessageSchema);
