const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const SessionSchema = new Schema({
  id: {
    type: Number,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
});
module.exports = Session = mongoose.model("Session", SessionSchema);
